package com.example.reneita_1202160121_si4001_pab_modul1v2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edtAlas;
    private EditText edtTinggi;
    private TextView edtHitung;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void hitung(View view){
        edtAlas = findViewById(R.id.alas_editText);
        edtTinggi =  findViewById(R.id.tinggi_editText2);
        edtHitung = findViewById(R.id.hasil_textView2);

        Integer alas = Integer.parseInt(edtAlas.getText().toString());
        Integer tinggi = Integer.parseInt(edtTinggi.getText().toString());
        Integer hasil = alas*tinggi;
        edtHitung.setText(String.valueOf(hasil));
    }

}

